# ZBLOG插件模板说明仓库
- 此仓库用于储存插件的说明文档
- 如有问题请联系作者邮箱：9258405@qq.com

## 关于作者

- 作者名称： 川南吴彦祖
- 邮箱： 9258405@qq.com
- 介绍： 五年Web开发经验，可接正规网页、小程序、APP、插件开发
- 售后群： 793577886(加群请带上应用中心名称)

## 插件列表

#### 多功能云储存

[说明文档](https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/MengdeFilePro/index.md)		[更新日志](https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/MengdeFilePro/log.md)		[应用下载](https://app.zblogcn.com/?id=22917)

#### 保存远程图片(本地化、CDN、采集、存储)

[说明文档](https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/CNWYZImageLocal/index.md)		[更新日志](https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/CNWYZImageLocal/log.md)

