# 多功能云储存
## 基本信息

#### 售后QQ群

*  793577886(加群请带上订单号)
* 更多插件：[点击直达](https://gitee.com/cnwuyanzu/zblog-plug-in-collection)

#### 功能简介

1. 新附件管理：批量删除、批量上传、预览视频、预览图片、预览音频、分类、复制外链
2. 文章编辑：编辑器新增操作按钮，可直接管理已存在的附件(可以直接预览)、直接插入附件(按附件形式、按各自格式插入)、批量上传
3. 搬迁云储存：支持本地附件同步到设定好的云储存、支持同步文章附件地址，可实现：云储存链接替换为本地链接，本地链接替换为云储存链接，云储存链接替换为其他云储存链接
4. 云盘文件分享：可以为云储存、也可以为本地，分享给别人附件地址，打开后输入密码直接可以查看分享的文件列表

#### 注意事项

1. 目前只支持PHP7.3及以上版本
2. 目前文章编辑页面兼容三款编辑器：官方自带、TinyMCE 1.0.2、Neditor编辑器，如果需要支持其他的编辑器，购买后联系开发者添加
3. 目前云储存支持：
   1. 又拍云
   2. 七牛云 (  <font color="red">0Kb</font>  文件无法上传，免费空间只能使用 <font color="red">华南地区</font> 能上传，其他地区请自测)
   3. FTP传输
   4. 如需更多云储存联系开发者添加。


#### 环境要求(安装说明)

1. <font color="red">PHP7.3</font>以上(必须)
2. PHP必须安装 <font color="red">fileinfo</font> 扩展(必须)
3. 使用云盘分享功能，开启 <font color="red">伪静态</font> (配合该插件:   [静态管理中心1.6](https://app.zblogcn.com/?id=234)  )，需要设置网站的伪静态
4. 安装好后必须给  <font color="red">插件目录755权限</font>  (宝塔->网站根目录->zb_users/plugin/MengdeFilePro 给这个整个目录755权限 必须要有写入权限)

#### 伪静态配置

###### Nginx伪静态配置

```
if (!-e $request_filename){
    rewrite  ^/zb_users/plugin/MengdeFilePro/public/md.php/(.*)$  /zb_users/plugin/MengdeFilePro/public/md.php?s=$1  last;
}
if (-f $request_filename/index.html){
	rewrite (.*) $1/index.html break;
}
if (-f $request_filename/index.php){
	rewrite (.*) $1/index.php;
}
if (!-f $request_filename){
	rewrite (.*) /index.php;
}
```

###### Apache伪静态配置

```
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule '$' ^/zb_users/plugin/MengdeFilePro/public/md.php/(.*)$ [L] 
RewriteRule . /index.php [L]
</IfModule>
```

## 更新日志

- [更新日志](https://gitee.com/cnwuyanzu/zblog-plug-in-collection/blob/master/plugin/MengdeFilePro/log.md)

## 图文介绍

####  文章操作

###### 批量插入附件

###### 批量上传

###### 按格式插入附件

![批量上传按格式插入附件](https://mengde-image.wolai.kim/file_article.gif)



###### 快捷批量复制外链

![](https://mengde-image.wolai.kim/note/20211112155107.png)

 



#### 独立附件管理

###### 附件列表、附件预览

![附件列表、附件预览](https://mengde-image.wolai.kim/file_list.gif)









###### 批量上传、批量删除

![独立附件管理,批量上传、批量删除](https://mengde-image.wolai.kim/file_cz.gif)







#### 同步管理

###### 同步文章附件地址

###### 同步本地附件至云储存

![同步设置、同步文章附件地址、同步本地附件至云储存](https://mengde-image.wolai.kim/file_cloud_setting.gif)

###### 新增FTP类型同步

![新增FTP类型同步](https://mengde-image.wolai.kim/note/20211115002525.png)



#### 云盘功能

###### 分享设置

![分享设置](https://mengde-image.wolai.kim/file_share.gif)

